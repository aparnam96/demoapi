/**
 * general
 *
 * @author aparna
 * @date 9/17/19
 **/
component extends="handlers.BaseHandler"{

   function authFailed(event, rc, prc){
   	  var HTTPVerb = event.getHTTPMethod();
   	  if(HTTPVerb != "OPTIONS"){
      //prc.response.setData(GetHttpRequestData().Headers);
         onAuthorizationFailure(event,rc,prc);
   	  	}
   }

   function invalidHTTPMethod(event, rc , prc, targetAction, eventArguments)
   {
        onInvalidHTTPMethod(event, rc, prc, prc.targetAction, prc.eventArguments);
   }
   function missingAction(event, rc , prc, targetAction, eventArguments)
   {
        onMissingAction(event, rc , prc, prc.missingAction, prc.eventArguments);
   }
   function tokenError(event,rc,prc)
   {
        onTokenError(event, rc, prc);
   }
   function invalidResource(event, rc, prc)
   {
   	  onInvalidResouce(event, rc, prc);
   }
   function customError(event, rc, prc)
   {
   	 var result = "error";
   	 prc.response.setData(result);
   }
}