/**
* I am a new handler
*/
component extends= "handlers.BaseHandler"{
  // Dependency Injection
    property name="contactService" inject="ContactService";
    property name="loginService" inject;
	// OPTIONAL HANDLER PROPERTIES
	this.prehandler_only 	= "";
	this.prehandler_except 	= "";
	this.posthandler_only 	= "";
	this.posthandler_except = "";
	this.aroundHandler_only = "";
	this.aroundHandler_except = "";

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		view='GET',
		remove='DELETE',
		save='POST,PUT'
	};


    function preHandler( event, rc, prc, targetAction, eventArguments)
    {
    	//prc.response.addHeader("Access-Control-Allow-Origin","*");
        var currentURL = event.getCurrentRoutedURL();
        var HTTPVerb = event.getHTTPMethod();
        var Method = ListToArray(currentURL, "/");
        var action = Method[3];
         prc.targetAction = action;
         prc.eventArguments = eventArguments;
         //check for refresh token
        // variables.refreshToken = 0;
//         if(prc.refreshTokenFlag == 1)
//         {
//         	prc.refreshToken = "refreshToken :"+loginService.generate(now(),1,0);
//         }
        switch(action)
        {
        	case "view":
	        	if(HTTPVerb!= "GET")
	        	{
                   event.overrideEvent( 'api.general.invalidHTTPMethod' );
	            }
	        	break;
        	case "save":
	        	if(HTTPVerb != "PUT" && HTTPVerb != "POST" )
	        	{
	             event.overrideEvent( 'api.general.invalidHTTPMethod' );
	        	}
	        	break;
        	case "remove":
	        	if(HTTPVerb != "DELETE")
	        	{
	              event.overrideEvent( 'api.general.invalidHTTPMethod' );
	        	}
	        	break;
        	default:
        	    prc.missingAction = action;
	        	event.overrideEvent( 'api.general.missingAction' );
	        	break;
        }
    }
	/**
	* Index
	*/
	any function index( event, rc, prc ){
		prc.response.setData( "Welcome to my ColdBox RESTFul Service" );
	}
	/**
	* List All Contacts
	*/
	any function view( event, rc, prc ){
		rc.format="json";
	    prc.response.setData( contactService.getAll() );
	}

	/**
	* Save A Contact
	*/
	any function save( event, rc, prc ){

	    var sContacts = contactService.save();
	    prc.response.setData( sContacts );
	}

	/**
	* Delete An Existing Contact
	*/
	any function remove( event, rc, prc ){

	    var sContacts = contactService.remove( Form.contactID );
	    prc.response.setData( sContacts );
	}


}
