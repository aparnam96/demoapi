/**
 * logout
 *
 * @author aparna
 * @date 10/14/19
 **/
component extends= "handlers.BaseHandler" {
	property name="loginService" inject;

	function dologout(event, rc, prc)
	{
		//var userId = 1;
        var result = loginService.dologoutAction(prc.userId);
		var result = 1;
        prc.response.setData(result);
	}

}