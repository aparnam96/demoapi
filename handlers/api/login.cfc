/**
 * login
 *
 * @author aparna
 * @date 9/19/19
 **/
component extends="handlers.BaseHandler"{
	 property name="loginService" inject;

    function checkCredential(event, rc, prc)
    {
    	if(rc.Username == "aparnamaurya@gmail.com" && rc.Password == "Aparna123")
    	{
            var result = generate(event, rc, prc);
    		prc.response.setData(result);
    	}
    	else
    	{
            event.overrideEvent('api.general.authFailed');
    	}

    }
	function generate (event, rc, prc)
	{
		var now = now();
		var insert = loginService.addwindowId(event,rc , prc, now);
		if(insert.isDone)
		{
		var result = loginService.generateToken(now,0,insert.windowId);
		}
		else if(insert.errorMessage == "")
		{
			result["error"] = true;
			result["errorMessage"]= "You are already logged in from other tab or device";
		}
		else
		{
            result["error"] = true;
            result["errorMessage"]= insert.errorMessage;
		}
        return result;

	}

}