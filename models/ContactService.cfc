/**
* I am a new Model Object
*/
component accessors="true"{

	// Properties
	property name="contacts" type="string";	property name="lastID" type="string";

	/**
	 * Constructor
	 */
	ContactService function init(){
	    varcontacts = StructNew();
		varlastID = 10;
		return this;
	}

		/**
		* Get all contacts
		*/
		struct function getAll(){
            var  result = ArrayNew(1);
			queryObj = new query();
			queryObj.setDatasource("Sample");
			queryObj.setName("QueryResult");
			var  queryResult = queryObj.execute(sql="SELECT * FROM Person");
			var  contactQuery = Queryresult.getResult();
			varcount = Queryresult.getPrefix().recordcount;
            var  i =1;
            for (row in contactQuery) {

                 varcontactId = i;
                 varcontacts[ varcontactID ]["ContactId"] = row.PersonID;
                 varcontacts[ varcontactID ]["FirstName"] = row.FirstName;
                 varcontacts[ varcontactID ]["LastName"] = row.SurName;
                 varcontacts[ varcontactID ]["Email"] = row.Email;
                 varcontacts[ varcontactID ]["PhoneNumber"] = row.PhoneNumber;
                 i++;
			 }
		    return varcontacts;
		}

		/**
		* save and return all contacts
		* @contactID The id to save/update
		* @data The data record
		*/
		struct function save( ){
		    // insert, move lastID
		   if(Form.contactID == 0)
		   {
		   	try{
		   	//insert new row
			   	queryObj = new query();
	            queryObj.setDatasource("Sample");
	            queryObj.setName("QueryResult");
	            queryObj.addParam(name="firstName",value="#Form.FirstName#",cfsqltype="VARCHAR");
	            queryObj.addParam(name="surName",value="#Form.SurName#",cfsqltype="VARCHAR");
	            queryObj.addParam(name="Email",value="#Form.Email#",cfsqltype="VARCHAR");
	            queryObj.addParam(name="PhoneNumber",value="#Form.PhoneNumber#",cfsqltype="VARCHAR");
	            queryObj.addparam(name="Password",value="#Form.Password#",cfSqltype="VARCHAR");
	            var  Queryresult = queryObj.execute(sql="INSERT into Person(FirstName, SurName, Email, PhoneNumber, Password) Values(:firstName,:surname, :Email, :PhoneNumber, :Password)");
	            varcontactQuery = Queryresult.getResult();
	            varcount = Queryresult.getPrefix();
	            if(count.RecordCount == 1)
	            {
	            	result.success = "Contact is added .Id is"+count.generatedKey;
	            }
            }
            catch(any e)
            {
            	result.error ="Database Error Occured";
            }
		   }
		   else{
		   	    try{
		   	    queryObj = new query();
                queryObj.setDatasource("Sample");
                queryObj.setName("QueryResult");
                queryObj.addParam(name="contactId",value="#Form.contactID#",cfsqltype="NUMERIC");
                queryObj.addParam(name="firstName",value="#Form.FirstName#",cfsqltype="VARCHAR");
                queryObj.addParam(name="surName",value="#Form.SurName#",cfsqltype="VARCHAR");
                queryObj.addParam(name="Email",value="#Form.Email#",cfsqltype="VARCHAR");
                queryObj.addParam(name="PhoneNumber",value="#Form.PhoneNumber#",cfsqltype="VARCHAR");
                queryObj.addparam(name="Password",value="#Form.Password#",cfSqltype="VARCHAR");
                var  Queryresult = queryObj.execute(sql="Update Person
                                          Set FirstName = :firstName,
                                          SurName =:surName,
                                          Email =:Email,
                                          PhoneNumber=:PhoneNumber,
                                          Password =:Password
                                          WHERE PersonID= :contactID");
                varcontactQuery = Queryresult.getResult();
                varcount = Queryresult.getPrefix();
                  result.success= "Contact is updated";
		   	    }
		   	    catch(any e)
		   	    {
		   	    	result.error = "contact ID does not Exist";
		   	    }

		   }
		   return result;

		}

		/**
		* Remove a contact
		* @contactID The incoming ID
		*/
		struct function remove( contactID ){
			var  result = StructNew();
          try{
	            queryObj = new query();
	            queryObj.setDatasource("Sample");
	            queryObj.setName("QueryResult");
	            queryObj.addParam(name="contactId",value="#arguments.contactID#",cfsqltype="NUMERIC");
	            var  Queryresult = queryObj.execute(sql="DELETE FROM Person WHERE PersonID = :contactId");
	            var  contactQuery = Queryresult.getResult();
	            result.success = "Successfully deleted";
          	}
            catch(any e)
            {
            	result.error = "This id does not exist";

            }
            return result;

		}

}