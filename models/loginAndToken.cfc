/**
 * loginAndToken
 *
 * @author aparna
 * @date 10/13/19
 **/
component accessors=true output=false persistent=false {

	function addwindowId(event,rc,prc, now)
	{
	   try{
            //create window ID
            var windowID = createUUID();
            //Save in DB
            var message.isDone = 1;
            message.windowId= windowID;
            queryObj = new query();
            queryObj.setDatasource("Sample");
            queryObj.setName("QueryResult");
            queryObj.addParam(name="userId",value=1,cfsqltype="NUMERIC");
            queryObj.addParam(name="windowID",value=windowID,cfsqltype="VARCHAR");
            queryObj.addParam(name="isLogged",value=1,cfsqltype="BIT");
            queryObj.addParam(name="time" ,value=arguments.now, cfsqltype="timestamp");
            var userExistQuery = queryObj.execute(sql = "SELECT * FROM sessionTable WHERE userID =:userId");
            var loginInfo = userExistQuery.getResult();
            var userExist = userExistQuery.getPrefix().recordcount;
            if(userExist != 1)
            {
               var Queryresult = queryObj.execute(sql="INSERT into sessionTable(userId, windowID, isLogged,tokenCreationTime)
                                                         Values(:userId, :windowID, :isLogged, :time)");
            }
            else
            {
                var timeExceeds = DateDiff("n",loginInfo.tokenCreationTime,now());
                if( timeExceeds >=10)
                {


                  var Queryresult = queryObj.execute(sql="Update sessionTable
                                              Set tokenCreationTime = :time,
                                                  windowID = :windowID
                                              WHERE userId= :userId");
                }
                else
                {
                	 message.isDone = 0;
                	 message.errorMessage="";
                }
            }
          //return message;
        }

        catch(any e)
        {
            message.isDone = 0;
            message.errorMessage = e.message;

        }
        return message;

	}

  function generateToken(now ,update, windowId){
  	 //create expiration of token
  	  var date = now;
      var extend = 10*60; //add 10 min for token expire.
      var extendedDate = DateAdd("s",extend,date);
      var epoch = DateDiff("s",DateConvert("utc2Local", "January 1 1970 00:00"), extendedDate);
        //payload
      var payload = {"ts" = now,"exp"= epoch,"userId" = 1};
       // encode the payload
      var token = application.jwt.encode(payload);
  	  if(arguments.update == 1)
  	  {
  	 	updateTokenCreationTime(now);
  	 	var result = token;
  	  }
  	  else
  	  {
        var result = {"token": token, "expiryTime":extend, "windowID" : windowID};
  	  }
      return result;
  }
  function updateTokenCreationTime(now)
  {
  	 queryObj = new query();
     queryObj.setDatasource("Sample");
     queryObj.setName("QueryResult");
     queryObj.addParam(name="userId",value=1,cfsqltype="NUMERIC");
     queryObj.addParam(name="isLogged",value=1,cfsqltype="BIT");
     queryObj.addParam(name="time" ,value=arguments.now, cfsqltype="timestamp");
     var Queryresult = queryObj.execute(sql="Update sessionTable
                                              Set tokenCreationTime = :time
                                              WHERE userId= :userId AND isLogged = :isLogged");

  }
  function dologoutAction(userid)
  {
  	try{
	  	 queryObj = new query();
	     queryObj.setDatasource("Sample");
	     queryObj.setName("QueryResult");
	     queryObj.addParam(name="userId",value=arguments.userid,cfsqltype="NUMERIC");

	     var Queryresult = queryObj.execute(sql="DELETE FROM sessionTable WHERE userId= :userId");
         return 1;
  	   }
  	catch(any e)
  	{
  		return 0;
  	}

  }

}