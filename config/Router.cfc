component{

	function configure(){
		// Set Full Rewrites
		setFullRewrites( true );

		/**
		 * --------------------------------------------------------------------------
		 * App Routes
		 * --------------------------------------------------------------------------
		 *
		 * Here is where you can register the routes for your web application!
		 *
		 *
		 */

         route( "/api/contacts/:contactID")
         .withAction( {
                GET    = 'view',
                OPTIONS = 'view',
                POST   = 'save',
                PUT    = 'save',
                DELETE = 'remove'
            } )
        .toHandler("api.contacts");
        route("/api/login").toHandler("api.login");
        route("/api/general").toHandler("api.general");
        route("/api/logout").toHandler("api.logout");
		// A nice healthcheck route example
		route("/healthcheck",function(event,rc,prc){
			return "Ok!";
		});

		// Conventions based routing
		route( ":handler/:action?" ).end();
	}

}