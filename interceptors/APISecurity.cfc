/**
 * APISecurity
 *
 * @author aparna
 * @date 9/17/19
 **/
component accessors=true output=false persistent=false
	hint="Intercepts all the events for api and does security checks."{

   function preProcess( event, interceptData, buffer )   {
       var routedURL = event.getCurrentRoutedURL();
       if(routedURL != "api/login/")
       {
           //check for authorization parameter in the http header
	       if(StructkeyExists(getHttpRequestData().headers,"authorization"))
	       {
	       	  //access token from the header
		       var token= (ListLast(getHttpRequestData().headers.authorization, " "));
		       if(token != "")
		       {
		           try
		            {
		              //var windowtoken = ListLast(token,"!");
		             // if(windowtoken != "" || windowtoken != "undefined")
		              //{
		              	//  var hastoken = ListFirst(token,"!");


		              	//check the token is valid or not
				          var payload = application.jwt.decode(token);
				         // if token is valid add further level of authentication.
				          prc.userId = payload.userId;


				            //queryObj = new query();
				            //queryObj.setDatasource("Sample");
				            //queryObj.setName("QueryResult");
				            //queryObj.addParam(name="userID",value=userId,cfsqltype="NUMERIC");
				            //var Queryresult = queryObj.execute(sql="SELECT * FROM sessionTable WHERE userId = :userID");
				            //var row = Queryresult.getResult();

				           //if(windowtoken == row.windowID && row.isLogged == 1)
				           //{
				           	 // checking resources from url
				           	 if(routedURL ==  "api/logout/")
				           	 {
				           	 	event.overrideEvent('api.logout.dologout');
				           	 }
				           	 //var timeLeftForTokenExpires = dateDiff("m", now(), payload.ts);
				           	 //if(timeLeftForTokenExpires <= 4)
				           	 //{
				           	 	//prc.refreshTokenFlag = 1;
				           	 //}

                              //check for routed resource.
	                          if(routedURL != "api/contacts/view/" && routedURL != "api/contacts/save/" && routedURL != "api/contacts/remove/" && routedURL != "api/logout/")
	                            {
	                              prc.message = "Resource not found";
	                              event.overrideEvent('api.general.invalidResource');
	                            }
				           //}
				           //else
				           //{
                             //event.overrideEvent('api.general.authFailed');
				           //}


		              //	}
//		              	else
//		              	{
//		              		event.overrideEvent('api.general.authFailed');
//		              	}
			        }
			        catch(any e)
			        {
			          prc.message= "InvalidToken";
			          event.overrideEvent('api.general.tokenError');
			        }


		       	}
		       	else
		       	{
		       	prc.message = "Invalid Access: token is empty";
		       	event.overrideEvent('api.general.tokenError');
		       	}
	       }
	       else
	       {
	       	    event.overrideEvent( 'api.general.authFailed' );
	       }

	 }
   	else
   	{
       //Login To api
	   // Check the credentials of user

         event.overrideEvent('api.login.checkCredential');

   	}

}
}