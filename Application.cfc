﻿/**
* Copyright 2005-2007 ColdBox Framework by Luis Majano and Ortus Solutions, Corp
* www.ortussolutions.com
* ---
*/
component{
	// Application properties
	this.name = hash( getCurrentTemplatePath() );
	this.sessionManagement = true;
	this.sessionTimeout = createTimeSpan(0,0,30,0);
	this.setClientCookies = true;
	//this.appSettings = getApplicationSettings();
    //this.datasources = appSettings.datasources ?: {};
    this.datasource= "Sample";
	//this.datasources["Sample"] = {
         // required
       // class: 'com.microsoft.jdbc.sqlserver.SQLServerDriver',
	    //connectionString: 'jdbc:sqlserver://APARNA-PC;databaseName=Sample;SelectMethod=direct;sendStringParametersAsUnicode=true;querytimeout=0;MaxPooledStatements=1000;failoverPartner=SERVER\INSTANCE',
	    //username: 'sa',
	    //password: 'mindfire'
   // };
     //application action='update' datasources=this.datasources;
     THIS.rootPath = GetDirectoryFromPath(GetCurrentTemplatePath());
     this.mappings[ '/coldbox' ] = '#THIS.rootPath#/coldbox'

	// COLDBOX STATIC PROPERTY, DO NOT CHANGE UNLESS THIS IS NOT THE ROOT OF YOUR COLDBOX APP
	COLDBOX_APP_ROOT_PATH = getDirectoryFromPath( getCurrentTemplatePath() );
	// The web server mapping to this application. Used for remote purposes or static purposes
	COLDBOX_APP_MAPPING   = "";
	// COLDBOX PROPERTIES
	COLDBOX_CONFIG_FILE 	 = "";
	// COLDBOX APPLICATION KEY OVERRIDE
	COLDBOX_APP_KEY 		 = "";



	// application start
	public boolean function onApplicationStart(){
        application.jwt = createobject('component','models.jwt').init();
		application.cbBootstrap = new coldbox.system.Bootstrap( COLDBOX_CONFIG_FILE, COLDBOX_APP_ROOT_PATH, COLDBOX_APP_KEY, COLDBOX_APP_MAPPING );


		application.cbBootstrap.loadColdbox();


		return true;
	}

	// application end
	public boolean function onApplicationEnd( struct appScope ){
		arguments.appScope.cbBootstrap.onApplicationEnd( arguments.appScope );
	}

	// request start
	public boolean function onRequestStart( string targetPage ){


		// Process ColdBox Request

		application.cbBootstrap.onRequestStart( arguments.targetPage );

		return true;
	}

	public void function onSessionStart(){
		application.cbBootStrap.onSessionStart();
	}

	public void function onSessionEnd( struct sessionScope, struct appScope ){
		arguments.appScope.cbBootStrap.onSessionEnd( argumentCollection=arguments );
	}

	public boolean function onMissingTemplate( template ){
		return application.cbBootstrap.onMissingTemplate( argumentCollection=arguments );
	}

}